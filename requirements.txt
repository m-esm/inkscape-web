
# Universal requirements
unidecode
python-dateutil==2.7.3
whoosh==2.7.4
pygments==2.2.0
django==1.11.15
django-ajax-selects==1.7.0
django-haystack==2.8.1
django-contrib-comments==1.8.0
django-markdown-deux==1.0.5
django-boxed-alerts==1.3.5
django-ical==1.7.0
django-recurrence==1.10.3

# cms
django-cms==3.7.3
djangocms-file==2.4.0
cmsplugin-search==0.8.0
cmsplugin-alerts==1.2.2
cmsplugin-diff==1.2.3
djangocms-text-ckeditor==3.9.1

# download_remote_images command
beautifulsoup4==4.6.0

# person - User, login and registration
django-registration==3.0
social-auth-app-django==2.1.0
python3-vote-core==20170329.0
django-nocaptcha-recaptcha==0.0.20
stopforumspam==1.8

# gallery
python-gnupg==0.4.4

# Fixing pot files
polib==1.1.0

# Test only
django-extratest>=1.6
pylint==1.9.2
pylint-django==0.11.1

# Live only
#psycopg2-binary==2.7.5
#uwsgi==2.0.17
#xapian-haystack==2.1.1
#user-agents==2.2.0
