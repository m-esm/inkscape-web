# -*- coding: utf-8 -*-
#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""Urls for Calendars"""

from django.conf.urls import url
from inkscape.url_utils import url_tree
from person import user_urls, team_urls

from .views import EventList, TeamEventFeed, TeamEvent, TeamEventList

team_urls.urlpatterns += [
    url(r'^calendar.ics$', TeamEventFeed(), name='team_calendar_feed'),
    url(r'^calendar/$', TeamEventList.as_view(), name='team_calendar'),
]

urlpatterns = [
    url(r'^$', EventList.as_view(), name='full'),
    url(r'^inkscape.ics$', TeamEventFeed(), name='full_feed'),
    url(r'^event/(?P<pk>\d+)/$', TeamEvent.as_view(), name='event'),
]
