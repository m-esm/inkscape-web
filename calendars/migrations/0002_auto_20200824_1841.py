# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2020-08-24 18:41
from __future__ import unicode_literals

from django.db import migrations
import recurrence.fields


class Migration(migrations.Migration):

    dependencies = [
        ('calendars', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='recurrences',
            field=recurrence.fields.RecurrenceField(blank=True, null=True),
        ),
    ]
