#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Base statistics views, include these in your stats pages.
"""

import re
from collections import defaultdict, OrderedDict
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta

import pytz

from django.db.models import Count, Sum, Q
from django.db.models.functions import TruncDay
from django.utils.timezone import now
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _
from django.template import Context, TemplateDoesNotExist
from django.template.loader import get_template
from django.forms.widgets import Media

from .utils import doy, Week, InvalidData, NoData, clean_numbers

class Statistics(list):
    """A list of statistics"""
    @property
    def media(self):
        ret = Media()
        for stat in self:
            ret += stat.media
        return ret

class StatisticBase(object):
    """Statistics base module"""
    template_name = None
    default_template = None
    STATS = defaultdict(list)
    title = None

    class Media:
        js = ()
        css = {}

    get_name = classmethod(lambda cls: re.sub('([A-Z]+)', r'_\1', cls.__name__).strip('_').lower())
    get_app = classmethod(lambda cls: cls.__module__.split('.')[0])
    media = property(lambda self: Media(self.Media))

    def __init_subclass__(cls):
        if 'Base' not in cls.__name__:
            StatisticBase.STATS[cls.get_app()].append(cls)

    def __init__(self, options):
        self.options = options

    def get_template_names(self, name=None):
        """Get a list of template filenames in resolution order"""
        app = self.get_app()
        if name is None:
            name = self.get_name()
        return [name for name in [self.template_name, f"{app}/stats/{name}.html",
                                  self.default_template] if name is not None]

    def get_context_data(self, **kwargs):
        """Gathers the data, checks it's in the right format and returns it"""
        try:
            kwargs.update(self.validate(self.filter_data(self.get_data())))
        except NoData:
            kwargs['nodata'] = True
        return kwargs

    def get_data(self):
        raise NotImplementedError("You must provide a get_data function!")

    def filter_data(self, data):
        return data

    def validate(self, data):
        if not isinstance(data, dict):
            raise InvalidData("Data must be a dictionary.")
        return data

    def render(self):
        """Render the stats into it's template"""
        found_name = "None"
        errors = []
        tmpl = None

        for name in self.get_template_names():
            try:
                tmpl = get_template(name)
                found_name = name
                break
            except TemplateDoesNotExist as error:
                errors.append(str(error))

        if tmpl is None:
            raise TemplateDoesNotExist(', '.join(errors))

        return mark_safe(tmpl.render(
            self.get_context_data(
                name=self.get_name(),
                title=self.title,
                found_name=found_name)
        ))

    def annotation(self):
        """How the date ranges are counted"""
        return Count('id')


class UserListBase(StatisticBase):
    """A list of users shown as user icons, ordered by a metric"""
    default_template = 'stats/user_list.html'

class DateCountBase(StatisticBase):
    """A chart showing counting items"""
    default_template = 'stats/date_count.html'
    category_fields = []
    minimum_cadence = 'day'
    trunc_func = TruncDay
    chart_type = 'line'
    chart_options = {}

    # Loose definitions for time scales
    time_scales = {
        'date': None,
        'year': relativedelta(years=1),
        'quarter': relativedelta(months=3),
        'month': relativedelta(months=1),
        'week': relativedelta(days=7),
        'day': relativedelta(days=1),
    }
    # This is needed because relativedelta is incomparable
    _times = ['year', 'quarter', 'month', 'week', 'day']

    class Media:
        js = (
            'chartist/chartist.min.js',
            'chartist/tooltip.min.js',
            'chartist/legendary.js',
            'chartist/units.js',
        )
        css = {'all': (
            'chartist/chartist.min.css',
            'chartist/tooltip.css',
            'chartist/legendary.css',
            'chartist/units.css',
        )}

    def get_cadence(self, qset):
        """Get a suitable cadence for this graph"""
        name = self.options.get('cadence', 'week')
        if not isinstance(name, str):
            name = name[0]
        cadence = name
        if self._times.index(name) > self._times.index(self.minimum_cadence):
            cadence = self.minimum_cadence

        return self.time_scales.get(cadence), name

    def filter_data(self, qset):
        """Turn a datetime, count series into the right output for a date count"""
        cadence, cadence_name = self.get_cadence(qset)

        ends = {} # {'from': datetime, 'to': datetime}
        dt_filter = Q()
        for opr, suffix in (('gte', 'from'), ('lte', 'to')):
            if suffix in self.options:
                if '-' in self.options[suffix]:
                    self.options[f'date_{suffix}'] = self.options[suffix]
                else:
                    for scale_name in self.time_scales:
                        self.options.pop(f"{scale_name}_{suffix}", None)
                    self.options[f'{cadence_name}_{suffix}'] = self.options[suffix]

            for scale_name in self.time_scales:
                dtm = self.userdata_to_datetime(
                    self.time_scales[scale_name],
                    self.options.get(f"{scale_name}_{suffix}", None),
                )
                opt = self.options.get(f"{scale_name}_{suffix}", None)
                if dtm is not None:
                    ends[suffix] = self.align_date(dtm.date(), cadence)
                    dt_filter &= Q(**{self.date_field + '__' + opr: dtm})
                    break

        qset = qset.filter(dt_filter)
        qset = qset.annotate(stat_date=self.trunc_func(self.date_field))\
            .values("stat_date", *self.category_fields)\
            .annotate(count=self.annotation())\
            .order_by("stat_date")

        categories = set()
        results = []
        for item in qset:
            category = ''
            if self.category_fields:
                category = item[self.category_fields[0]]
            categories.add(category)
            dtm = self.align_date(item.pop('stat_date'), cadence)
            results.append((dtm, item.pop('count'), category))

        if not results:
            raise NoData("No data available")

        series = OrderedDict()
        dt = ends.get('from', results[0][0])
        dt_to = ends.get('to', results[-1][0])
        while dt <= dt_to:
            series[dt] = None
            new_dt = self.align_date(dt + cadence, cadence)
            if dt == new_dt:
                break
            dt = new_dt

        ret = OrderedDict()
        categories = list(sorted(categories))
        for cat in categories:
            ret[cat] = series.copy()

        for dt, count, cat in results:
            if dt in series:
                if ret[cat][dt] is None:
                    ret[cat][dt] = 0
                ret[cat][dt] += count

        return {
            'labels': self.space_labels(list(series)),
            'series': [(cat, list(vals.values())) for cat, vals in ret.items()],
            'chart': self.chart_type,
            'options': self.chart_options,
        }

    def userdata_to_datetime(self, scale, data):
        """Sanitise user data into a scaled datetime (from now)"""
        if data is None:
            return None
        if isinstance(data, list):
            data = data[0]
        try:
            if scale is None and isinstance(data, str):
                unaware = datetime.strptime(data, "%Y-%m-%d")
                dtm = pytz.utc.localize(unaware)
            else:
                dtm = now() - (scale * int(data))
            if not isinstance(dtm, datetime):
                raise ValueError("Not a datetime!")
        except ValueError:
            # Catch bad ints, bad dates and other value errors
            return None
        return dtm

    def space_labels(self, labels, count=8):
        """Limit the number of labels shown"""
        size = len(labels)
        every = int(size / count)
        ret = []
        for i in range(size):
            if not every or i in (0, size-1) or not i % every:
                ret.append(str(labels[i]))
            else:
                ret.append('')
        return ret

    def align_date(self, dt, cadence):
        """Align the date to a specific cadience"""
        if isinstance(dt, datetime):
            dt = dt.date()
        if cadence == self.time_scales['day']: # Daily
            return dt
        elif cadence == self.time_scales['week']: # Weekly
            return Week(dt)
        elif cadence == self.time_scales['month']:
            return date(dt.year, dt.month, 1) # Monthly
        elif cadence == self.time_scales['year']:
            return date(dt.year, 1, 1) # Yearly
        return dt

    @property
    def date_field(self):
        raise NotImplementedError("You must provide a date_field.")


class DateSumBase(DateCountBase):
    """Like DateCount but sums up a field instead"""
    @property
    def count_field(self):
        raise NotImplementedError("You must provide a count_field.")

    def annotation(self):
        """How the date ranges are counted"""
        return Sum(self.count_field)

class CategorySumBase(DateSumBase):
    """Use the category as the axis and ignore dates"""
    category_label = 'Data'

    def get_category_label(self, cat):
        return cat

    def filter_data(self, qset):
        results = super().filter_data(qset)
        pre = [(cat, {
                 'value': sum(clean_numbers(row)),
                 'meta': self.get_category_label(cat),
               }) for cat, row in results['series']]
        pre.sort(key=lambda r: r[1]['value'], reverse=True)
        results['labels'], results['series'] = zip(*pre)
        results['series'] = [(self.category_label, results['series'])]
        return results

class WorldMapBase(StatisticBase):
    """Export as a map instead"""
    default_template = 'stats/world_map.html'
    country_field = "country"
    count_field = "count"

    class Media:
        js = (
            'js/jquery-ui.js',
        )
        css = {'all':(
            'css/jquery-ui.css',
        )}

    def get_category_label(self, cat):
        return cat

    def filter_data(self, qset):
        qset = qset.values(self.country_field)\
                   .annotate(count=Sum(self.count_field))\
                   .order_by(self.count_field)
        ret = OrderedDict()
        total = 0
        for item in qset:
            total += item['count']
            ret[item[self.country_field]] = {
                'value': item['count'],
                'meta': self.get_category_label(item[self.country_field]),
            }

        return {
            'total': total,
            'values': ret,
        }

